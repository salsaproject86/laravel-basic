<?php

namespace App\View\Components\Partial;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Delete extends Component
{
    public $url;
    public $id;

    public function __construct($url, $id)
    {
        $this->url = $url;
        $this->id = $id;
    }

    public function render(): View|Closure|string
    {
        return view('components.partial.delete');
    }
}
