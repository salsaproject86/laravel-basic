<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
Use InvalidArgumentException;

class AuthController extends Controller
{
    function index(Request $req){
       
        try{
       /*untuk mencari data user berdasarkan email */
            $find = User::where('email', $req->email)->first();
       /*untuk cek inputan email dan password */
            // return dd($find);
       /**
        * untuk mengecek apa user ditemukan atau tidak
        * jika tidak ditemukan maka akan muncul notifikasi di bawah
        */
            if(!$find){
                throw new InvalidArgumentException('Akun tidak ditemukan',500);
            }
       /**untuk mengecek kecocokan yang ada di dalam database */
            if(!Hash::check($req->password, $find->password)){
            throw new InvalidArgumentException('Email atau Password salah !',500);
          }
          /**
           * Auth:: untuk akses login
           * sebagai "web"
           */
          Auth::guard('web')->login($find, $remember = true);

          /**
           * untuk otomatis berpindah halaman ke 'dashboard'
           */
          return redirect('dashboard');

        } catch(\Throwble $th){
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ],500 );
         }
    }
    function logout(Request $request){
        /**
         * untuk menghapus login yang masuk sebgai 'web'
         */
         Auth::guard('web')->logout();
        
         $request->session()->invalidate();
         $request->session()->regenerateToken();

         return redirect('/');
    }

    function loginPage(){
        /**
         * untuk memvalidasi
         * jika sudah login akan diarahkan ke 'dashboard'
         */
        if(auth()->user()){
            return redirect('dashboard');
        }
        return view('login');
    }
    function profile(){
        $item = User::find(auth()->user()->id);

        if (!$item) {
          return abort(404);
        }
    
        return view('user.edit', [
          'item' => $item,
          'profile' => true
        ]);
    }
}
