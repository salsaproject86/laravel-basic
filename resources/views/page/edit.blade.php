<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Artikel</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?used=2607')}}" />
</head>
<body>

  <x-page.form  :item="$item" actionurl="pages/update"/>

  
</body>
</html>