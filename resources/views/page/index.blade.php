<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Artikel</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?used=2607')}}" />
</head>
<body>

  <x-page.form />

  <h4>Daftar Artikel :</h4>

  <table>
    <thead>
      <tr>
        <td style= "width : 50px" >No</td>
        <td  style= "width : 250px" >Judul</td>
        <td  style= "width : 150px" >Kategori</td>
        <td >Gambar</td>
        <td>Singkat</td>
        <td  style= "width : 100px" >Aksi</td>
      </tr>
    </thead>
    <tbody>
      @foreach ($collection as $number => $item)
        <tr>
          <td>{{$number + 1}}</td>
          <td>{{$item->title}}</td>
          <td>{{$item->category?->title ?? '-'}}</td>
          <td>
            <img 
              src="{{asset($item->image)}}"
              alt="image" style="width: 100px" />
          </td>
          <td>
            {{Str::words($item->description, 20)}}
          </td>
          <td>
            <a href = "{{url('pages/edit/' .$item->id)}}">Edit</a>
              <x-partial.delete url="pages/delete" id="{{$item->id}}"/>
          </td>
       
        </tr>
      @endforeach
    </tbody>
  </table>
  
</body>
</html>